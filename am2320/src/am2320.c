#include <stddef.h>

#include "../am2320.h"
#include "api/delay.h"

void am2320_getTemperatureAndHumidity(i2c_t* i2c, uint8_t address, int16_t* temperature, uint16_t* humidity)
{
    static const uint8_t cmd[3] = { 0x03, 0x00, 0x04 };
    uint8_t data[8];
    i2c_write(i2c, address, NULL, 0);
    delay_ms(1);
    i2c_write(i2c, address, cmd, sizeof(cmd));
    delay_ms(2);
    i2c_read(i2c, address, data, sizeof(data));
    uint16_t temp_temperature = (data[5] | data[4] << 8);

    if (temp_temperature & 0x8000) {
        *temperature = -(int16_t)(temp_temperature & 0x7fff);
    } else {
        *temperature = (int16_t)temp_temperature;
    }
    *humidity = (data[3] | data[2] << 8);
}

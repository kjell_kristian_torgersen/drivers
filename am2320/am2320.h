#ifndef DRIVERS_AM2320_AM2320_H_
#define DRIVERS_AM2320_AM2320_H_

#include <stdint.h>

#include "api/i2c.h"

void am2320_getTemperatureAndHumidity(i2c_t* i2c, uint8_t address, int16_t* temperature, uint16_t* humidity);

#endif /* DRIVERS_AM2320_AM2320_H_ */

#include "../lcd.h"

#include "api/delay.h"
#include "api/i2c.h"

#define LCD_PIN_RS (1 << 0)
#define LCD_PIN_EN (1 << 2)
#define LCD_BACKLIGHT (1 << 3)
#define LCD_DELAY_MS 5

#define LCD_CLEAR 0b00000001
#define LCD_FUNCTION_SET 0b00110000
#define LCD_ON 0b00001100
#define LCD_RETURN_HOME 0b00000010

bool lcd_write(lcd_t* this, uint8_t data, uint8_t flags)
{
    bool ready = false;
    for (int i = 0; i < 10; i++) {
        bool ready = i2c_deviceIsReady(this->i2c, this->address, 1);
        if (ready)
            break;
    }
    if (!ready)
        return false;
    uint8_t up = data & 0xF0;
    uint8_t lo = (data << 4) & 0xF0;

    uint8_t buf[4];
    buf[0] = up | flags | LCD_BACKLIGHT | LCD_PIN_EN;
    buf[1] = up | flags | LCD_BACKLIGHT;
    buf[2] = lo | flags | LCD_BACKLIGHT | LCD_PIN_EN;
    buf[3] = lo | flags | LCD_BACKLIGHT;

    int res = i2c_write(this->i2c, this->address, buf, sizeof(buf));
    delay_ms(LCD_DELAY_MS);
    return res > 0;
}

void lcd_sendCommand(lcd_t* this, uint8_t cmd)
{
    lcd_write(this, cmd, 0);
}

void lcd_sendData(lcd_t* this, uint8_t data)
{
    lcd_write(this, data, LCD_PIN_RS);
}

static void lcd_inithw(lcd_t* this)
{
    lcd_sendCommand(this, LCD_FUNCTION_SET);
    lcd_sendCommand(this, LCD_RETURN_HOME);
    lcd_sendCommand(this, LCD_ON);
    lcd_sendCommand(this, LCD_CLEAR);
}

void lcd_init(lcd_t* this, i2c_t* i2c, uint8_t address)
{
    if (this) {
        this->address = address;
        this->i2c = i2c;
        this->idx = 0;
        lcd_inithw(this);
    }
}

void lcd_print(lcd_t* this, char* str)
{
    while (*str) {
        lcd_sendData(this, (uint8_t)(*str));
        str++;
    }
}

void lcd_putchar(lcd_t* this, char c)
{
    lcd_sendData(this, (uint8_t)(c));
    this->idx++;
    if ((this->idx & 15) == 0) {
        if (this->idx & 16) {

        } else {
            lcd_sendCommand(this, 0b10000000);
        }
    }
}

void lcd_locate(lcd_t* this, int x, int y)
{
    if (x & 1) {
        lcd_sendCommand(this, 0b11000000 + y);
    } else {
        lcd_sendCommand(this, 0b10000000 + y);
    }
    this->idx = y;
}

void lcd_cls(lcd_t* this)
{
    lcd_sendCommand(this, 0b00000001);
    lcd_sendCommand(this, 0b11000000);
    this->idx = 0;
}

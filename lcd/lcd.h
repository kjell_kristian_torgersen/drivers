#ifndef DRIVERS_LCD_LCD_H_
#define DRIVERS_LCD_LCD_H_
#include <stdint.h>

#include "api/i2c.h"

typedef struct lcd {
    i2c_t* i2c;
    uint8_t address;
    uint8_t idx;
} lcd_t;

void lcd_init(lcd_t* this, i2c_t* i2c, uint8_t address);
bool lcd_write(lcd_t* this, uint8_t data, uint8_t flags);
void lcd_sendCommand(lcd_t* this, uint8_t cmd);
void lcd_sendData(lcd_t* this, uint8_t data);
void lcd_init(lcd_t* this, i2c_t* i2c, uint8_t address);
void lcd_print(lcd_t* this, char* str);
void lcd_putchar(lcd_t* this, char c);
void lcd_cls(lcd_t* this);
void lcd_locate(lcd_t* this, int x, int y);

#endif /* DRIVERS_LCD_LCD_H_ */

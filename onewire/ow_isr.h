#ifndef C16FCEA0_8D3D_45D1_BF6C_BB513C69DDE1
#define C16FCEA0_8D3D_45D1_BF6C_BB513C69DDE1

#include <stdbool.h>
#include <stdint.h>

typedef bool (*ow_isr_done_t)(uint8_t* buf, int count, void* arg);

typedef struct ow_isr {
    const uint8_t* txbuf;
    uint8_t* rxbuf;

    ow_isr_done_t callback;
    void* callback_arg;

    uint8_t port;
    uint8_t pin;
    uint8_t state;
    uint8_t total_bytes;
    uint8_t current_byte;
    uint8_t bit_count;
} ow_isr_t;

void ow_isr_init(ow_isr_t* this, uint8_t port, uint16_t pin);

/** \brief Call this from a timer interrupt. Use the return value to set when next interrupt should happen (in us) */
int ow_isr_update(ow_isr_t* this);

/** \brief Transmit and receive data. First a reset is performed. Then data is both transmitted and received at same time.
 * to receive transmit 0xFF for those bytes */
int ow_isr_txrx(ow_isr_t* this, const uint8_t* txbuf, uint8_t* rxbuf, int count, ow_isr_done_t callback, void* callback_arg);

#endif /* C16FCEA0_8D3D_45D1_BF6C_BB513C69DDE1 */

#include "main.h"

#include "api/delay.h"
#include "api/gpio.h"
#include "api/mcu.h"
#include "api/onewire.h"

#define ONEWIRE_SEARCH_ROM 0xF0

// The different recommended delays for onewire
#define A 6
#define B 64
#define C 60
#define D 10
#define E 9
#define F 55
#define G 0
#define H 480
#define I 70
#define J 410

// Set bus low
#define LOW()                                  \
    gpio_clear_pinmask(this->port, this->pin); \
    gpio_config(this->port, this->pin, GPIO_OUTPUT);

// Set bus high
#define HIGH()                               \
    gpio_set_pinmask(this->port, this->pin); \
    gpio_config(this->port, this->pin, GPIO_OUTPUT);

// Release bus (to high)
#define RELEASE() gpio_config(this->port, this->pin, GPIO_INPUT_HIGH)

// Read bit
#define READ() gpio_get_pinmask(this->port, this->pin)

void onewire_init(onewire_t* this, uint8_t port, uint8_t pin)
{
    if (this) {
        this->port = port;
        this->pin = 1 << pin;
        RELEASE();
    }
}

onewire_status_t onewire_reset(onewire_t* this)
{
    bool presence;
    bool bus_low;
    // delay_us(G);

    LOW();
    delay_us(H);
    RELEASE();
    delay_us(I);
    presence = !READ();
    delay_us(J);
    bus_low = !READ();

    if (bus_low) {
        return OW_BUS_LOW;
    } else if (presence) {
        return OW_OK;
    } else {
        return OW_NO_PRESENCE;
    }
}

ret_t onewire_handle_id(onewire_t* this, const uint8_t* id)
{
    if (id) {
        const uint8_t cmd[] = { ONEWIRE_MATCH_ROM };
        onewire_write(this, cmd, sizeof(cmd));
        onewire_write(this, id, 8);
    } else {
        const uint8_t cmd[] = { ONEWIRE_SKIP_ROM };
        onewire_write(this, cmd, sizeof(cmd));
    }
    return RET_OK;
}

void onewire_writeBit(onewire_t* this, bool bit)
{
    mcu_disableIRQs();
    if (bit) {
        LOW();
        delay_us(A);
        HIGH();
        delay_us(B);
    } else {
        LOW();
        delay_us(C);
        HIGH();
        delay_us(D);
    }
    RELEASE();
    mcu_enableIRQs();
}

bool onewire_readBit(onewire_t* this)
{
    bool bit;
    mcu_disableIRQs();
    LOW();
    delay_us(A);
    RELEASE();
    delay_us(E);
    bit = READ();
    // gpio_toggle_pinmask(2, 1<<12);
    delay_us(F);
    mcu_enableIRQs();
    return bit;
}

void onewire_writeByte(onewire_t* this, uint8_t byte)
{
    for (int i = 0; i < 8; i++) {
        onewire_writeBit(this, byte & 0x01);
        byte >>= 1;
    }
}

ret_t onewire_write(onewire_t* this, const void* data, unsigned count)
{
    const uint8_t* p = (const uint8_t*)data;
    for (unsigned i = 0; i < count; i++) {
        onewire_writeByte(this, p[i]);
    }
    return RET_OK;
}

ret_t onewire_power(onewire_t* this, bool enable)
{
    if (enable) {
        gpio_config(this->port, this->pin, GPIO_OUTPUT);
    } else {
        gpio_config(this->port, this->pin, GPIO_INPUT_HIGH);
    }
    return RET_OK;
}

uint8_t onewire_readByte(onewire_t* this)
{
    uint8_t byte = 0;

    for (int i = 0; i < 8; i++) {
        byte >>= 1;
        if (onewire_readBit(this)) {
            byte |= 0x80;
        }
    }
    return byte;
}

ret_t onewire_read(onewire_t* this, void* data, unsigned count)
{
    for (int i = 0; i < count; i++) {
        ((uint8_t*)data)[i] = onewire_readByte(this);
    }
    return count;
}

uint8_t onewire_readWriteByte(onewire_t* this, uint8_t outByte)
{
    uint8_t inByte = 0;

    for (int i = 0; i < 8; i++) {
        inByte >>= 1;
        if (outByte & 0x01) {
            if (onewire_readBit(this)) {
                inByte |= 0x80;
            }
        } else {
            onewire_writeBit(this, 0);
        }

        outByte >>= 1;
    }
    return inByte;
}

int onewire_transceive(onewire_t* this, void* data, int count)
{
    for (int i = 0; i < count; i++) {
        ((uint8_t*)data)[i] = onewire_readWriteByte(this, ((uint8_t*)data)[i]);
    }
    return count;
}

int onewire_transceive2(onewire_t* this, const void* dataOut, void* dataIn, int count)
{
    for (int i = 0; i < count; i++) {
        ((uint8_t*)dataIn)[i] = onewire_readWriteByte(this, ((const uint8_t*)dataOut)[i]);
    }
    return count;
}

void onewire_command(onewire_t* this, uint8_t command, uint8_t* id)
{
    uint8_t i;

    onewire_reset(this);

    if (id) {
        onewire_writeByte(this, ONEWIRE_MATCH_ROM); // to a single device
        i = 8;
        do {
            onewire_writeByte(this, *id);
            id++;
        } while (--i);
    } else {
        onewire_writeByte(this, ONEWIRE_SKIP_ROM); // to all devices
    }

    onewire_writeByte(this, command);
}

uint8_t onewire_search_rom(onewire_t* this, uint8_t diff, uint8_t* id)
{
    uint8_t i, j, nextDiff;
    bool b;

    if (onewire_reset(this)) {
        return ONEWIRE_PRESENCE_ERROR;
    }

    onewire_writeByte(this, ONEWIRE_SEARCH_ROM);
    nextDiff = ONEWIRE_LAST_DEVICE;

    i = ONEWIRE_ROMCODE_SIZE * 8;

    do {
        j = 8;
        do {
            b = onewire_readBit(this);
            if (onewire_readBit(this)) {
                if (b) {
                    return ONEWIRE_DATA_ERROR;
                }
            } else {
                if (!b) {
                    if (diff > i || ((*id & 1) && diff != i)) {
                        b = true;
                        nextDiff = i;
                    }
                }
            }
            onewire_writeBit(this, b);
            *id >>= 1;
            if (b) {
                *id |= 0x80;
            }

            i--;

        } while (--j);
        id++;
    } while (i);

    return nextDiff;
}

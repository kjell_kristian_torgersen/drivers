#include <stddef.h>
#include <stdint.h>

#include "../ow_isr.h"
#include "api/gpio.h"

typedef enum ow_state {
    OW_RESET_BEGIN,
    OW_RESET_END,
    OW_PRESENCE,
    OW_WRITE_BEGIN,
    OW_WRITE_END,
    OW_READ_BIT_BEGIN,
    OW_FINALIZE_BIT,
    OW_READY_NEXT_BIT,
    OW_DONE

} ow_state_t;

#define HIGH()                                           \
    gpio_config(this->port, this->pin, GPIO_INPUT_HIGH); \
    gpio_set_pinmask(this->port, this->pin);

#define LOW()                                  \
    gpio_clear_pinmask(this->port, this->pin); \
    gpio_config(this->port, this->pin, GPIO_OUTPUT);

//#define READ() gpio_get_pinmask(this->port, this->pin)

bool READ(ow_isr_t* this)
{
    bool ret = gpio_get_pinmask(this->port, this->pin) > 0;
    gpio_toggle_pinmask(2, 1 << 12);
    return ret;
}

void ow_isr_init(ow_isr_t* this, uint8_t port, uint16_t pin)
{
    this->txbuf = NULL;
    this->rxbuf = NULL;
    this->state = OW_DONE;
    this->port = port;
    this->pin = 1 << pin;
    HIGH();
    gpio_config(2, 1 << 12, GPIO_OUTPUT);
}

int ow_isr_update(ow_isr_t* this)
{
    switch (this->state) {
    case OW_RESET_BEGIN:
        LOW();
        this->state = OW_RESET_END;
        return 480;
    case OW_RESET_END:
        HIGH();
        this->state = OW_PRESENCE;
        return 70; // TODO:
    case OW_PRESENCE:
        if (READ(this)) { // no presence
            if (this->callback)
                this->callback(NULL, 0, this->callback_arg);
            this->state = OW_DONE;
        } else {
            this->state = OW_WRITE_BEGIN;
        }
        return 410;
    case OW_WRITE_BEGIN:
        LOW();
        this->state = OW_WRITE_END;
        return 6;
    case OW_WRITE_END:
        if (this->txbuf[this->current_byte] & (1 << this->bit_count)) {
            HIGH();
        } else {
            LOW();
        }
        this->state = OW_READ_BIT_BEGIN;
        return 9 - 6;
    case OW_READ_BIT_BEGIN:
        // gpio_togglePin(2, 13);
        if (this->txbuf[this->current_byte] & (1 << this->bit_count)) {
            if (this->rxbuf) {
                if (!READ(this)) {
                    this->rxbuf[this->current_byte] &= ~(1 << this->bit_count);
                } else {
                    this->rxbuf[this->current_byte] |= (1 << this->bit_count);
                }
            }
        }
        // gpio_clearPin(2, 13);
        this->state = OW_FINALIZE_BIT;
        return 45 + 6;
    case OW_FINALIZE_BIT:
        HIGH();
        this->state = OW_READY_NEXT_BIT;
        return 10;
    case OW_READY_NEXT_BIT:
        this->bit_count++;
        if (this->bit_count >= 8) {
            this->bit_count = 0;
            this->current_byte++;
            if (this->current_byte == this->total_bytes) {
                if (this->callback) {
                    if (this->callback(this->rxbuf, this->total_bytes, this->callback_arg)) {
                        this->state = OW_RESET_BEGIN;
                    } else {
                        this->state = OW_DONE;
                    }
                } else {
                    this->state = OW_DONE;
                }
            } else {
                this->state = OW_WRITE_BEGIN;
            }
        } else {
            this->state = OW_WRITE_BEGIN;
        }
        return 10;
    case OW_DONE:
        return 0;
    }

    return 0;
}

int ow_isr_txrx(ow_isr_t* this, const uint8_t* txbuf, uint8_t* rxbuf, int count, ow_isr_done_t callback, void* callback_arg)
{
    this->rxbuf = rxbuf;
    this->txbuf = txbuf;
    this->total_bytes = count;
    this->current_byte = 0;
    this->bit_count = 0;
    this->callback = callback;
    this->callback_arg = callback_arg;
    this->state = 0;
    return 0;
}

#include "config.h"

#ifdef OW_NUM_BUSES
#include "../ow.h"
#include "api/delay.h"
#include "api/gpio.h"
#include "api/mcu.h"
#include <stddef.h>

#define LOW()                             \
    gpio_clearPin(this->port, this->pin); \
    gpio_config(this->port, this->pin, GPIO_OUTPUT)

#define HIGH()                          \
    gpio_setPin(this->port, this->pin); \
    gpio_config(this->port, this->pin, GPIO_INPUT)

struct ow {
    uint8_t port;
    uint8_t pin;
    int8_t parasitic;
};

ow_t ow[OW_NUM_BUSES] = { 0 };

ow_t* ow_init(uint8_t port, uint8_t pin)
{
    for (int i = 0; i < OW_NUM_BUSES; i++) {
        if (ow[i].parasitic == 0) {
            ow[i].port = port;
            ow[i].pin = pin;
            ow[i].parasitic = 1;
            // ow[i].parasitic = parasitic ? 1 : -1;

            return &ow[i];
        }
    }
    return NULL;
}

ow_status_t ow_reset(ow_t* this)
{
    if (!this)
        return OW_NULL_POINTER;
    ow_status_t status = OW_OK;
    HIGH();
    if (!(gpio_getPin(this->port, this->pin)))
        return OW_BUS_LOW;
    LOW();
    delay_us(480);
    HIGH();
    delay_us(70);

    if (!(gpio_getPin(this->port, this->pin)))
        status = OW_OK; // If pin is low, it means the device responded
    else
        status = OW_NO_PRESENCE;

    delay_us(410); // Get 480 us delay totally

    return status;
}

int ow_write(ow_t* this, const void* buf, int count)
{
    if (!this)
        return -1;
    const uint8_t* data = buf;
    for (int j = 0; j < count; j++) {
        for (int i = 0; i < 8; i++) {
            // check if bit is high or low
            mcu_disableIRQs();
            if ((data[j] & (1 << i)) != 0) {

                // write 1
                LOW();
                delay_us(6);
                HIGH();
                delay_us(64);
            } else {
                // write 0
                LOW();
                delay_us(60);
                HIGH();
                delay_us(10);
            }
            mcu_enableIRQs();
        }
    }
    return count;
}

int ow_read(ow_t* this, void* buf, int count)
{
    if (!this)
        return -1;
    uint8_t* data = buf;
    HIGH();
    for (int j = 0; j < count; j++) {
        data[j] = 0;
        mcu_disableIRQs();
        for (int i = 0; i < 8; i++) {

            LOW();
            delay_us(3);
            HIGH();
            delay_us(9);
            if (gpio_getPin(this->port, this->pin)) {
                // pin is high
                data[j] |= 1 << i; // read = 1
            } else {
                // pin is low
                volatile int x = 7;
                x++;
            }
            delay_us(50 + 5 + 3);
        }
        mcu_enableIRQs();
    }
    return count;
}

void ow_powerOn(ow_t* this)
{
    if (!this)
        return;
    gpio_setPin(this->port, this->pin);
    gpio_config(this->port, this->pin, GPIO_OUTPUT);
}

void ow_powerOff(ow_t* this)
{
    if (!this)
        return;
    gpio_config(this->port, this->pin, GPIO_INPUT);
}

#endif

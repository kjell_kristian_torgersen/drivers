#include <stdbool.h>
#include <stdint.h>

#include "../ds18b20.h"

#include "api/delay.h"
#include "api/gpio.h"
#include "api/onewire.h"

enum ds18b20_cmds {
    DS18B20_READ_ROM = 0x33,
    DS18B20_CONVERT_T = 0x44,
    DS18B20_READ_SCRATCHPAD = 0xBE,
};

#define DS18S20_FAMILY_CODE 0x10
#define DS18B20_FAMILY_CODE 0x28
#define DS1822_FAMILY_CODE 0x22

onewire_status_t ds18b20_start_convert_t(onewire_t* ow, const void* id, bool parasitic)
{
    const uint8_t cmd[] = { DS18B20_CONVERT_T };
    onewire_status_t status = onewire_reset(ow);
    if (status != OW_OK) {
        return status;
    }
    delay_ms(1);
    onewire_handle_id(ow, id);
    onewire_write(ow, cmd, sizeof(cmd));

    if (parasitic) {
        onewire_power(ow, true);
    }
    return status;
}

onewire_status_t ds18b20_get_temperature(onewire_t* ow, const void* id, ds18b20_data_t* data)
{
    uint8_t cmd[] = { DS18B20_READ_SCRATCHPAD };
    onewire_status_t status = onewire_reset(ow);
    if (status != OW_OK) {
        return status;
    }
    delay_ms(1);
    onewire_handle_id(ow, id);
    onewire_write(ow, cmd, sizeof(cmd));
    onewire_read(ow, data, sizeof(*data));

    return OW_OK;
}

onewire_status_t ds18b20_get_rom(onewire_t* ow, uint64_t* data)
{
    uint8_t cmd[] = { DS18B20_READ_ROM };
    onewire_status_t status = onewire_reset(ow);
    if (status != OW_OK) {
        return status;
    }

    delay_ms(1);
    onewire_write(ow, cmd, sizeof(cmd));
    onewire_read(ow, data, sizeof(*data));

    return OW_OK;
}

static onewire_status_t ds18b20_find_sensor(onewire_t* this, uint8_t* diff, uint8_t* id)
{
    uint8_t running;
    onewire_status_t ret;

    ret = OW_OK;
    running = 1;
    do {
        *diff = onewire_search_rom(this, *diff, &id[0]);
        if ((*diff == ONEWIRE_PRESENCE_ERROR) || (*diff == ONEWIRE_DATA_ERROR) || (*diff == ONEWIRE_LAST_DEVICE)) {
            running = 0;
            ret = RET_ERROR;
        } else {
            if ((id[0] == DS18B20_FAMILY_CODE) || (id[0] == DS18S20_FAMILY_CODE) || (id[0] == DS1822_FAMILY_CODE)) {
                running = 0;
            }
        }
    } while (running);

    return ret;
}

onewire_status_t ds18b20_search_bus(onewire_t* this, void* ids, int max_ids)
{
    uint8_t i;
    uint8_t id[8];
    uint8_t* sensors = ids;
    uint8_t diff, sensorsFound;

    onewire_status_t status = onewire_reset(this);
    if (status != OW_OK) {
        return status;
    }
    sensorsFound = 0;

    diff = ONEWIRE_SEARCH_FIRST;
    while (diff != ONEWIRE_LAST_DEVICE && sensorsFound < max_ids) {
        ds18b20_find_sensor(this, &diff, &id[0]);

        if (diff == ONEWIRE_PRESENCE_ERROR) {
            break;
        }

        if (diff == ONEWIRE_DATA_ERROR) {
            break;
        }

        for (i = 0; i < ONEWIRE_ROMCODE_SIZE; i++)
            sensors[sensorsFound * 8 + i] = id[i];

        sensorsFound++;
    }

    return sensorsFound;
}
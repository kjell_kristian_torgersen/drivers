#ifndef DRIVERS_DS18B20_DS18B20_H_
#define DRIVERS_DS18B20_DS18B20_H_

#include <stdbool.h>

#include "api/onewire.h"

typedef struct ds18b20_data {
    int16_t temperature;
    int16_t thtl;
    uint8_t configuration;
    uint8_t reserved[3];
    uint8_t crc8;
} __attribute__((__packed__)) ds18b20_data_t;

typedef struct ds18b20 {
    uint8_t port;
    uint8_t pin;
} ds18b20_t;

onewire_status_t ds18b20_start_convert_t(onewire_t* ow, const void* id, bool parasitic);
onewire_status_t ds18b20_get_temperature(onewire_t* ow, const void* id, ds18b20_data_t* data);
onewire_status_t ds18b20_get_rom(onewire_t* ow, uint64_t* data);
onewire_status_t ds18b20_search_bus(onewire_t* this, void* ids, int max_ids);

#endif /* DRIVERS_DS18B20_DS18B20_H_ */
